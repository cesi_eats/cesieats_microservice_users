require('dotenv').config();
import logger from 'jet-logger';
import server from './server';
import connection from "./models/mysql";



// Constants
const serverStartMsg = 'Express server started on port: ',
        port = (process.env.PORT || 3000);

const start = async (): Promise<void> => {
    try {
        await connection.sync();
        console.log("Connected to mysql database");
        server.listen(port, () => {
            logger.info(serverStartMsg + port);
        });

    } catch (error) {
        console.error(error);
        process.exit(1);
    }
};
start();
