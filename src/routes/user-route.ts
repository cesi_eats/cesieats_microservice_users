/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';;
import usersService from "@services/users-service";


const baseRoute = "/auth";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    register: '/register',
    login: "/login",
    update: "/update",
    delete: "/delete",
    details: "/user",
    isValid: "/isValid",
    isAdmin: "/isAdmin",
    isDeliverer: "/isDeliverer",

} as const;

router.get(p.all, (req,res) => {
    usersService.getAll().then(data => {
        res.status(data.status).send(data);
    })
});

router.post(p.register, (req,res) => {
    if (!req.body.firstName || !req.body.password || !req.body.email || !req.body.name) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.register(req.body.firstName ,req.body.name, req.body.email, req.body.password).then(data => {
        res.status(data.status).send(data);
    })
});


router.post(p.login, (req,res) => {
    if (!req.body.email || !req.body.password) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.login(req.body.email, req.body.password).then(data => {
        res.status(data.status).send(data);
    })
});

router.put(p.update, (req,res) => {
    if (!req.body.email) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.update(req.body).then(data => {
        res.status(data.status).send(data);
    })
});

router.delete(p.delete, (req,res) => {
    if (!req.body.email) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.deleteUser(req.body.email).then(data => {
        res.status(data.status).send(data);
    })
});

router.post(p.details, (req,res) => {
    if (!req.body.token) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.getUserByToken(req.body.token).then(data => {
        res.status(data.status).send(data);
    })
});

router.post(p.isValid, (req,res) => {
    if (!req.body.token) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.isAuthenticated(req.body.token).then(data => {
        res.status(data.status).send(data);
    });
});

router.post(p.isAdmin, (req,res) => {
    if (!req.body.token) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.isAdmin(req.body.token).then(data => {
        res.status(data.status).send(data);
    });
});

router.post(p.isDeliverer, (req,res) => {
    if (!req.body.token) {
        res.status(400).send({ success: false, message: "Missing required fields" });
        return;
    }
    usersService.isAdmin(req.body.token).then(data => {
        res.status(data.status).send(data);
    });
});

// Export default
export default {
    router,
    baseRoute
} as const;
