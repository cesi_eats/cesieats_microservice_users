import { Router } from 'express';
import usersRouter from "@routes/user-route";

const apiRouter = Router();


apiRouter.use(usersRouter.baseRoute, usersRouter.router);


export default {
    apiRouter,
} as const;