import {User} from "../models/mysql/user-model";
import bcrypt from 'bcrypt';
import jwtUtil from '../utils/jwt-util';
import {Role} from "../models/mysql/role-model";
import sequelize from '../models/mysql/index';

interface CustomResponse{
    status : number,
    success : boolean,
    message : string,
    data? : object
}

async function getAll(){
    const users = await User.findAll({
        include:[
            {
                model: Role,
                attributes: ["roleName"],
            }] })
    return { status : 200, success: true, message: "Users found", data: users };
}

async function login(email: string, password: string): Promise<CustomResponse> {
    // Fetch user
    const user = await User.findOne({ where: { email : email } })
    if (!user) {
        return { status : 400, success: false, message: "User not found" };
    }
    const role = await user!.$get("role");

    // Check password
    const pwdPassed = await bcrypt.compare(password, user.password);

    if (!pwdPassed) {
        return { status : 400, success: false, message: "Invalid credentials" };
    }
    // Setup Admin Cookie
    let token =  await jwtUtil.sign({
        id: user.id,
        email: user.name,
        name: user.name,
        role: role?.roleName
    });
    return { status : 200, success: true, message : "authenticated", data: {token : token} };
}

async function register(firstName: string, name : string, email : string, password : string ) : Promise<CustomResponse> {
    try {
        if (await User.findOne({ where: { email : email } })) {
            return { status : 400, success: false, message: "Email already exists" };
        }
        const passwordHash = await bcrypt.hash(password, 10);
        const user = await User.create({ firstName, name, email, password: passwordHash });
        const role = await Role.findOne({ where: { roleName : "user" } })
        await user.$set("role", role)
        await user.save();
        return { status : 200, success: true, message: "User created" };
    }
    catch (e){
        return { status : 400, success: false, message: "Could not create user" };
    }
}


async function update(body : object) : Promise<CustomResponse> {

    // @ts-ignore
    const user = await User.findOne({ where: { email : body.email } })
    if (!user) {
        return { status : 400, success: false, message: "User not found" };
    }
    const id = user?.id;
    // @ts-ignore
    const { firstName, name, email, password } = body;
    let hashedPassword;
    if (password){
         hashedPassword = await bcrypt.hash(password, 10);
    }
    const updatedUser = await User.update({ firstName, name, email, password : hashedPassword }, { where: { id } });
    return { status : 200, success: true, message: "User updated" };
}

async function deleteUser(email : string){
    try {
        const user = await User.findOne({ where: { email : email } })
        if (!user) {
            return { status : 400, success: false, message: "User not found" };
        }
        await user.destroy();
        return { status : 200, success: true, message: "User deleted" };
    }
    catch (e){
        return { status : 400, success: false, message: "Could not delete user" };
    }
}

async function getUserByToken(token : string){
    try{
        let decoded = await jwtUtil.decode(token);
        // @ts-ignore
        if (!decoded || !decoded.id){
            return { status : 400, success: false, message: "Invalid token" };
        }
        // @ts-ignore
        const userId = decoded.id;
        const user = await User.findOne({ where: { id : userId } })
        if (!user) {
            return { status : 400, success: false, message: "User not found" };
        }
        return { status : 200, success: true, message: "User found", data: user };
    }
    catch (e){
        return { status : 400, success: false, message: "Invalid token" };
    }


}

async function isAuthenticated(token : string) : Promise<CustomResponse> {
    try {
        await jwtUtil.decode(token);
        return { status : 200, success: true, message: "authenticated" };
    }
    catch (e) {
        return { status : 400, success: false, message: "Invalid token" };
    }
}

async function isAdmin(token : string) : Promise<CustomResponse>{
    try {
        let decoded = await jwtUtil.decode(token);
        // @ts-ignore
        if (decoded.role === "admin") {
            return { status : 200, success: true, message: "admin" };
        }
        return { status : 403, success: false, message: "not admin" };
    }
    catch (e) {
        return { status : 400, success: false, message: "Invalid token" };
    }
}

async function isDeliverer(token : string) : Promise<CustomResponse>{
    try {
        let decoded = await jwtUtil.decode(token);
        // @ts-ignore
        if (decoded.role === "deliverer") {
            return { status : 200, success: true, message: "deliverer" };
        }
        return { status : 403, success: false, message: "not deliverer" };
    }
    catch (e) {
        return { status : 400, success: false, message: "Invalid token" };
    }
}

export default {
    getAll,
    login,
    register,
    update,
    deleteUser,
    getUserByToken,
    isAuthenticated,
    isAdmin
} as const;