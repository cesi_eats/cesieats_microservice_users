import { Sequelize } from "sequelize-typescript";
import {User} from "./user-model";
import {Role} from "./role-model";


const connection = new Sequelize({
    dialect: "mysql",
    host: process.env.MYSQL_HOST,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB,
    logging: false,
    models: [Role,User],
});


export default connection;
