import {Table, Model, Column, DataType, BelongsToAssociation, Association, BelongsTo} from "sequelize-typescript";
import {Role} from "./role-model";

@Table({
    timestamps: true,
    tableName: "users",
})

export class User extends Model {

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    firstName!: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    name!: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        defaultValue: true,
    })
    email!: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        defaultValue: true,
    })
    password!: string;

    @BelongsTo(() => Role, "roleId")
    role!: Role


}