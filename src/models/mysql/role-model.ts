import {Table, Model, Column, DataType, HasMany} from "sequelize-typescript";
import {User} from "./user-model";

@Table({
    timestamps: false,
    tableName: "roles",
})

export class Role extends Model {

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    roleName!: string;

    // @HasMany(() => User)
    // users

}